package com.polahniuk.model.pizzaria.pizza;

public enum Dough {
    ThickCrust, ThinCrust
}
