package com.polahniuk.model.pizzaria.pizza.impl;

import com.polahniuk.model.pizzaria.pizza.Pizza;
import com.polahniuk.model.pizzaria.recipe.Recipe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VeggiePizza implements Pizza {

    private Recipe recipe;
    private Logger log = LogManager.getLogger(VeggiePizza.class);

    public VeggiePizza(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public void prepare() {
        log.info("Prepare veggie pizza with recipe " + recipe);
    }

    @Override
    public void bake() {
        log.info("Bake veggie pizza");
    }

    @Override
    public void cut() {
        log.info("Cut all components for veggie pizza");
    }

    @Override
    public void box() {
        log.info("Box veggie pizza");
    }

    @Override
    public String toString() {
        return "Veggie pizza";
    }
}
