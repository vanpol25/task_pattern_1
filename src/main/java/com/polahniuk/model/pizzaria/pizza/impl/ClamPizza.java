package com.polahniuk.model.pizzaria.pizza.impl;

import com.polahniuk.model.pizzaria.pizza.Pizza;
import com.polahniuk.model.pizzaria.recipe.Recipe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ClamPizza implements Pizza {

    private Recipe recipe;
    private Logger log = LogManager.getLogger(ClamPizza.class);

    public ClamPizza(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public void prepare() {
        log.info("Prepare clam pizza with recipe " + recipe);
    }

    @Override
    public void bake() {
        log.info("Bake clam pizza");
    }

    @Override
    public void cut() {
        log.info("Cut all components for clam pizza");
    }

    @Override
    public void box() {
        log.info("Box clam pizza");
    }

    @Override
    public String toString() {
        return "Clam pizza";
    }

}
