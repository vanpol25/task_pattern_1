package com.polahniuk.model.pizzaria.pizza.impl;

import com.polahniuk.model.pizzaria.pizza.Pizza;
import com.polahniuk.model.pizzaria.recipe.Recipe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CheesePizza implements Pizza {

    private Recipe recipe;
    private Logger log = LogManager.getLogger(CheesePizza.class);

    public CheesePizza(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public void prepare() {
        log.info("Prepare cheese pizza with recipe " + recipe);
    }

    @Override
    public void bake() {
        log.info("Bake cheese pizza");
    }

    @Override
    public void cut() {
        log.info("Cut all components for cheese pizza");
    }

    @Override
    public void box() {
        log.info("Box cheese pizza");
    }

    @Override
    public String toString() {
        return "Cheese pizza";
    }
}
