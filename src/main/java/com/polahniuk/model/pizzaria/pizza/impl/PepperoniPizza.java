package com.polahniuk.model.pizzaria.pizza.impl;

import com.polahniuk.model.pizzaria.pizza.Pizza;
import com.polahniuk.model.pizzaria.recipe.Recipe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PepperoniPizza implements Pizza {
    private Recipe recipe;
    private Logger log = LogManager.getLogger(PepperoniPizza.class);

    public PepperoniPizza(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public void prepare() {
        log.info("Prepare pepperoni pizza with recipe " + recipe);
    }

    @Override
    public void bake() {
        log.info("Bake pepperoni pizza");
    }

    @Override
    public void cut() {
        log.info("Cut all components for pepperoni pizza");
    }

    @Override
    public void box() {
        log.info("Box pepperoni pizza");
    }

    @Override
    public String toString() {
        return "Pepperoni pizza";
    }
}
