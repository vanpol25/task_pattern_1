package com.polahniuk.model.pizzaria.recipe;

import com.polahniuk.model.pizzaria.pizza.Dough;

import java.util.Arrays;
import java.util.List;

public class Recipe {

    private Dough dough;
    private List<Sauce> sauces;
    private List<Topping> toppings;

    public Recipe(Dough dough, List<Sauce> sauces, List<Topping> toppings) {
        this.dough = dough;
        this.sauces = sauces;
        this.toppings = toppings;
    }

    public Recipe(Dough dough) {
        this.dough = dough;
    }

    public void addSauces(Sauce... sauces) {
        this.sauces = Arrays.asList(sauces);
    }

    public void addToppings(Topping... toppings) {
        this.toppings = Arrays.asList(toppings);
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "dough=" + dough +
                ", sauces=" + sauces +
                ", toppings=" + toppings +
                '}';
    }
}
