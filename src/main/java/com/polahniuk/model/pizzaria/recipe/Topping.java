package com.polahniuk.model.pizzaria.recipe;

public enum Topping {
    Pepperoni,
    Mushrooms,
    Onions,
    Sausage,
    Bacon,
    ExtraCheese,
    BlackOlives,
    GreenPeppers,
    Pineapple,
    Spinach
}
