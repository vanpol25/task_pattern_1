package com.polahniuk.model.pizzaria.recipe;

public enum Sauce {
    Marinara,
    PlumTomato,
    Pesto,
    Bechamel,
    BBQ,
    Pumpkin,
    Beet
}
