package com.polahniuk.model.pizzaria.bakery;

import com.polahniuk.model.pizzaria.PizzaType;
import com.polahniuk.model.pizzaria.pizza.Dough;
import com.polahniuk.model.pizzaria.pizza.Pizza;
import com.polahniuk.model.pizzaria.pizza.impl.CheesePizza;
import com.polahniuk.model.pizzaria.pizza.impl.ClamPizza;
import com.polahniuk.model.pizzaria.pizza.impl.PepperoniPizza;
import com.polahniuk.model.pizzaria.pizza.impl.VeggiePizza;
import com.polahniuk.model.pizzaria.recipe.Recipe;
import com.polahniuk.model.pizzaria.recipe.Sauce;
import com.polahniuk.model.pizzaria.recipe.Topping;

public class LvivBakery extends Bakery{

    @Override
    protected Pizza createPizza(PizzaType type) {
        Pizza pizza;
        if (type == PizzaType.Cheese) {
            Recipe recipe = new Recipe(Dough.ThickCrust);
            recipe.addSauces(Sauce.Bechamel, Sauce.Marinara);
            recipe.addToppings(Topping.ExtraCheese,
                    Topping.Pepperoni,
                    Topping.Bacon);
            pizza = new CheesePizza(recipe);
        } else if (type == PizzaType.Veggie) {
            Recipe recipe = new Recipe(Dough.ThinCrust);
            recipe.addSauces(Sauce.PlumTomato, Sauce.Pumpkin);
            recipe.addToppings(Topping.BlackOlives,
                    Topping.Spinach,
                    Topping.Pineapple,
                    Topping.GreenPeppers);
            pizza = new VeggiePizza(recipe);
        } else if (type == PizzaType.Pepperoni) {
            Recipe recipe = new Recipe(Dough.ThinCrust);
            recipe.addSauces(Sauce.PlumTomato, Sauce.Pesto, Sauce.Marinara);
            recipe.addToppings(Topping.GreenPeppers,
                    Topping.Mushrooms,
                    Topping.Pepperoni,
                    Topping.Bacon,
                    Topping.BlackOlives);
            pizza = new PepperoniPizza(recipe);
        } else if (type == PizzaType.Clam) {
            Recipe recipe = new Recipe(Dough.ThickCrust);
            recipe.addSauces(Sauce.PlumTomato, Sauce.Marinara);
            recipe.addToppings(Topping.GreenPeppers,
                    Topping.Mushrooms,
                    Topping.Pepperoni,
                    Topping.Bacon,
                    Topping.BlackOlives);
            pizza = new ClamPizza(recipe);
        } else {
            pizza = null;
        }
        return pizza;
    }

}
