package com.polahniuk.model.pizzaria.bakery;

import com.polahniuk.model.pizzaria.PizzaType;
import com.polahniuk.model.pizzaria.pizza.Pizza;

/**
 * Abstract factory.
 */
public abstract class Bakery {

    protected abstract Pizza createPizza(PizzaType type);

    public Pizza bake(PizzaType type) {
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.cut();
        pizza.bake();
        pizza.box();
        return pizza;
    }

}
