package com.polahniuk.model.pizzaria.bakery;

import com.polahniuk.model.pizzaria.PizzaType;
import com.polahniuk.model.pizzaria.pizza.Dough;
import com.polahniuk.model.pizzaria.pizza.Pizza;
import com.polahniuk.model.pizzaria.pizza.impl.*;
import com.polahniuk.model.pizzaria.recipe.*;

public class KyivBakery extends Bakery{

    @Override
    protected Pizza createPizza(PizzaType type) {
        Pizza pizza;
        if (type == PizzaType.Cheese) {
            Recipe recipe = new Recipe(Dough.ThickCrust);
            recipe.addSauces(Sauce.Bechamel, Sauce.Marinara);
            recipe.addToppings(Topping.ExtraCheese,
                    Topping.Onions,
                    Topping.Pineapple,
                    Topping.Pepperoni,
                    Topping.Bacon);
            pizza = new CheesePizza(recipe);
        } else if (type == PizzaType.Clam) {
            Recipe recipe = new Recipe(Dough.ThickCrust);
            recipe.addSauces(Sauce.PlumTomato, Sauce.Marinara);
            recipe.addToppings(Topping.GreenPeppers,
                    Topping.Mushrooms,
                    Topping.Pepperoni,
                    Topping.Bacon,
                    Topping.BlackOlives,
                    Topping.Sausage);
            pizza = new ClamPizza(recipe);
        } else if (type == PizzaType.Pepperoni) {
            Recipe recipe = new Recipe(Dough.ThinCrust);
            recipe.addSauces(Sauce.Pesto, Sauce.Marinara);
            recipe.addToppings(Topping.GreenPeppers,
                    Topping.Mushrooms,
                    Topping.Pepperoni,
                    Topping.Bacon,
                    Topping.BlackOlives);
            pizza = new PepperoniPizza(recipe);
        } else {
            pizza = null;
        }
        return pizza;
    }
}
