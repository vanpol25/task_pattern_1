package com.polahniuk.model.pizzaria;

public enum PizzaType {
    Cheese, Veggie, Clam, Pepperoni
}
