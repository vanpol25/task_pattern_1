package com.polahniuk.view;

import com.polahniuk.model.*;
import com.polahniuk.model.pizzaria.PizzaType;
import com.polahniuk.model.pizzaria.bakery.Bakery;
import com.polahniuk.model.pizzaria.bakery.DniproBakery;
import com.polahniuk.model.pizzaria.bakery.KyivBakery;
import com.polahniuk.model.pizzaria.bakery.LvivBakery;
import com.polahniuk.model.pizzaria.pizza.Pizza;
import org.apache.logging.log4j.*;

import java.util.*;

/**
 * Main view of project. Show menu in console.
 */
public class Menu {

    private Logger log = LogManager.getLogger(Menu.class);
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    private List<Pizza> pizzas;

    /**
     * Creates methods in constructor due to interface{@link Printable}.
     */
    public Menu() {
        setMenu();
        pizzas = new ArrayList<>();
        methods = new LinkedHashMap<>();
        methods.put("1", this::lvivBakery);
        methods.put("2", this::kyivBakery);
        methods.put("3", this::dniproBakery);
        methods.put("4", this::showOrder);
        show();
    }

    private void showOrder() {
        for (int i = 0; i < pizzas.size(); i++) {
            System.out.println((i + 1) + ". " + pizzas.get(i));
        }
    }

    private void lvivBakery() {
        System.out.println("Lviv bakery has such pizzas:" +
                "\n   1. Cheese pizza" +
                "\n   2. Veggie pizza" +
                "\n   3. Clam pizza" +
                "\n   4. Pepperoni pizza");
        System.out.println("Create your order:");
        List<PizzaType> types = new ArrayList<>();
        String i;
        do {
            i = sc.nextLine();
            if (i.equals("1")) {
                types.add(PizzaType.Cheese);
            } else if (i.equals("2")) {
                types.add(PizzaType.Veggie);
            } else if (i.equals("3")) {
                types.add(PizzaType.Clam);
            } else if (i.equals("4")) {
                types.add(PizzaType.Pepperoni);
            }
        } while (!i.toLowerCase().equals("q"));
        Bakery bakery = new LvivBakery();
        createOrder(bakery, types);
    }

    private void kyivBakery() {
        System.out.println("Lviv bakery has such pizzas:" +
                "\n   1. Cheese pizza" +
                "\n   2. Clam pizza" +
                "\n   3. Pepperoni pizza");
        System.out.println("Create your order:");
        List<PizzaType> types = new ArrayList<>();
        String i;
        do {
            i = sc.nextLine();
            if (i.equals("1")) {
                types.add(PizzaType.Cheese);
            } else if (i.equals("2")) {
                types.add(PizzaType.Clam);
            } else if (i.equals("3")) {
                types.add(PizzaType.Pepperoni);
            }
        } while (!i.toLowerCase().equals("q"));
        Bakery bakery = new KyivBakery();
        createOrder(bakery, types);
    }

    private void dniproBakery() {
        System.out.println("Lviv bakery has such pizzas:" +
                "\n   1. Cheese pizza" +
                "\n   2. Veggie pizza" +
                "\n   3. Pepperoni pizza");
        System.out.println("Create your order:");
        List<PizzaType> types = new ArrayList<>();
        String i;
        do {
            i = sc.nextLine();
            if (i.equals("1")) {
                types.add(PizzaType.Cheese);
            } else if (i.equals("2")) {
                types.add(PizzaType.Veggie);
            } else if (i.equals("3")) {
                types.add(PizzaType.Pepperoni);
            }
        } while (!i.toLowerCase().equals("q"));
        Bakery bakery = new DniproBakery();
        createOrder(bakery, types);
    }

    private void createOrder(Bakery bakery, List<PizzaType> types) {
        for (PizzaType type : types) {
            Pizza pizza = bakery.bake(type);
            pizzas.add(pizza);
        }
    }

    /**
     * Creating menu.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Lviv");
        menu.put("2", "2 - Kyiv");
        menu.put("3", "3 - Dnipro");
        menu.put("4", "4 - Show order");
        menu.put("Q", "Q - Exit");
    }

    /**
     * Show menu in console.
     */
    private void getMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    private void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select city point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

}